set -e

sudo apt-get install -y make wget zip
RELEASE=release0_9_12
SCHEME=Gauche-0.9.12
SCHEME_TAR="$SCHEME.tgz"

wget https://github.com/shirok/Gauche/releases/download/$RELEASE/$SCHEME_TAR -O $SCHEME_TAR
tar -xf $SCHEME_TAR
cd $SCHEME
mkdir ../gauche-install
./configure --prefix `pwd`/../gauche-install  && make && make install
cd ../gauche-install
zip -name "scheme_$RELEASE.zip" . -r
mv "scheme_$RELEASE.zip" ..
cd ..
rm -rf gauche-install $SCHEME $SCHEME_TAR
